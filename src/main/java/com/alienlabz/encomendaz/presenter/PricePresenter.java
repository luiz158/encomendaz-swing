package com.alienlabz.encomendaz.presenter;

import java.util.List;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

import br.gov.frameworkdemoiselle.stereotype.ViewController;

import com.alienlabz.encomendaz.bus.events.ShowPriceWindow;
import com.alienlabz.encomendaz.bus.qualifiers.CalculatePrice;
import com.alienlabz.encomendaz.bus.qualifiers.PostalServiceChanged;
import com.alienlabz.encomendaz.business.PostalServiceBC;
import com.alienlabz.encomendaz.business.ServiceBC;
import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.Package;
import com.alienlabz.encomendaz.domain.Person;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.BeanUtil;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.PriceForm;

/**
 * Responsável pelo tratamento de eventos oriundos da tela de cálculo de frete.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@ViewController
@Singleton
public class PricePresenter {

	private PriceForm priceForm;

	@Inject
	private TrackingBC trackingBC;

	@Inject
	private PostalServiceBC postalServiceBC;

	@Inject
	private ServiceBC serviceBC;

	/**
	 * Capturar e tratar o evento que solicita a exibição da janela de cálculo de frete.
	 * 
	 * @param event Evento.
	 */
	public void showPriceWindow(@Observes ShowPriceWindow event) {
		priceForm = new PriceForm();
		priceForm.pack();
		priceForm.setPostalServicesList(postalServiceBC.findAll());
		priceForm.setPostalService(postalServiceBC.getDefaultPostalService());
		SwingUtil.center(priceForm);
		priceForm.setVisible(true);
	}

	/**
	 * Capturar e tratar o evento que solicita o cálculo do valor do frete.
	 * 
	 * @param form Formulário.
	 */
	public void calculatePrice(@Observes @CalculatePrice final PriceForm form) {
		form.setBusy(true);
		new AsyncTask<Double, Void>() {

			@Override
			protected Double doTask() {
				Tracking tracking = new Tracking();
				tracking.setFrom(new Person());
				tracking.setTo(new Person());
				tracking.getFrom().setPostalCode(form.getFrom());
				tracking.getTo().setPostalCode(form.getTo());
				tracking.setService(form.getService());
				tracking.setDeclaredValue(form.getDeclaredValue());
				tracking.setWeight(form.getWeight());
				Package pack = new Package();
				BeanUtil.copyProperties(pack, form);
				tracking.setPack(pack);
				return trackingBC.getPrice(tracking);
			}

			@Override
			protected void onException(Throwable throwable) {
				form.setBusy(false);
			}

			@Override
			protected void onPostExecute(Double result) {
				form.setValue(result);
				form.setBusy(false);
			}

		}.execute();
	}

	/**
	 * Capturar e tratar o evento que informa uma mudança na lista de serviços postais.
	 * Esta mudança deve ser tratada para depois exibir a lista de serviços disponíveis.
	 * 
	 * @param event Evento.
	 */
	public void postalServiceChanged(@Observes @PostalServiceChanged final PriceForm priceForm) {
		if (priceForm.getPostalService() != null) {
			new AsyncTask<List<Service>, Void>() {

				@Override
				protected List<Service> doTask() {
					return serviceBC.findByPostalService(priceForm.getPostalService().getId());
				}

				protected void onPostExecute(java.util.List<Service> result) {
					priceForm.setServicesList(result);
				}

			}.execute();
		}
	}
}
