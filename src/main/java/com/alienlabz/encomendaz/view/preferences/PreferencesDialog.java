package com.alienlabz.encomendaz.view.preferences;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import java.util.ResourceBundle;

import javax.enterprise.util.AnnotationLiteral;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import org.pushingpixels.substance.api.skin.SkinInfo;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.qualifiers.Closed;
import com.alienlabz.encomendaz.domain.EmailServerSecurity;
import com.alienlabz.encomendaz.domain.EmailServers;
import com.alienlabz.encomendaz.factory.ResourceBundleFactory;
import com.alienlabz.encomendaz.util.IconUtil;

/**
 * Painel cujo objetivo é exibir as preferências do usuário para o sistema.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class PreferencesDialog extends JDialog {
	private static final long serialVersionUID = 1L;
	private JTabbedPane tabbedPane;
	private ResourceBundle bundleMessages;
	private ResourceBundle bundleImages;
	private PreferencesVisual panelPreferencesVisual;
	private PreferencesGeneral panelPreferencesGeneral;
	private PreferencesNetwork panelPreferencesNetwork;
	private PreferencesTwitter panelPreferencesTwitter;
	private PreferencesEmail panelPreferencesEmail;

	public PreferencesDialog() {
		bundleMessages = ResourceBundleFactory.getMessagesBundle();
		bundleImages = ResourceBundleFactory.getImagesBundle();
		initialize();
		initializeTabPane();
		initializeGeneralPreferences();
		initializeMailPreferences();
		initializeNetworkPreferences();
//		initializeTwitterPreferences();
		initializeVisualPreferences();
	}

	/**
	 * Inicializar a aba de preferências de temas.
	 */
	private void initializeVisualPreferences() {
		panelPreferencesVisual = new PreferencesVisual();
		tabbedPane.add(panelPreferencesVisual);

		JLabel label = new JLabel(bundleMessages.getString("preferences.tab.visual"));
		label.setIconTextGap(5);
		label.setPreferredSize(new Dimension(90, 100));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setVerticalTextPosition(SwingConstants.BOTTOM);
		label.setIcon(IconUtil.getImageIcon("visual_preferences.png"));
		tabbedPane.setTabComponentAt(3, label);
	}

	/**
	 * Inicializar a aba de preferências gerais.
	 */
	private void initializeGeneralPreferences() {
		panelPreferencesGeneral = new PreferencesGeneral();
		tabbedPane.add(panelPreferencesGeneral);

		JLabel label = new JLabel(bundleMessages.getString("preferences.tab.general"));
		label.setIconTextGap(5);
		label.setPreferredSize(new Dimension(90, 100));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setVerticalTextPosition(SwingConstants.BOTTOM);
		label.setIcon(IconUtil.getImageIcon(bundleImages.getString("preferences.tab.general.icon")));
		tabbedPane.setTabComponentAt(0, label);
	}

	/**
	 * Inicializar a aba de preferências de rede.
	 */
	private void initializeNetworkPreferences() {
		panelPreferencesNetwork = new PreferencesNetwork();
		tabbedPane.add(panelPreferencesNetwork);

		JLabel label = new JLabel(bundleMessages.getString("preferences.tab.network"));
		label.setIconTextGap(5);
		label.setPreferredSize(new Dimension(90, 100));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setVerticalTextPosition(SwingConstants.BOTTOM);
		label.setIcon(IconUtil.getImageIcon("network_preferences.png"));
		tabbedPane.setTabComponentAt(2, label);
	}

	/**
	 * Inicializar a aba de preferências do Twitter.
	 */
	private void initializeTwitterPreferences() {
		panelPreferencesTwitter = new PreferencesTwitter();
		tabbedPane.add(panelPreferencesTwitter);

		JLabel label = new JLabel(bundleMessages.getString("preferences.tab.twitter"));
		label.setIconTextGap(5);
		label.setPreferredSize(new Dimension(90, 100));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setVerticalTextPosition(SwingConstants.BOTTOM);
		label.setIcon(IconUtil.getImageIcon("twitter_preferences.png"));
		tabbedPane.setTabComponentAt(3, label);
	}

	/**
	 * Inicializar as opçõeses do painel de dialogo.
	 */
	private void initialize() {
		setLayout(new BorderLayout());
		setModal(true);
		setResizable(false);
		setPreferredSize(new Dimension(540, 640));
		setTitle(bundleMessages.getString("preferences.title"));
		addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent window) {
				EventManager.fire(PreferencesDialog.this, new AnnotationLiteral<Closed>() {
				});
			}

		});
	}

	/**
	 * Inicializar a aba de preferências de e-mail.
	 */
	private void initializeMailPreferences() {
		panelPreferencesEmail = new PreferencesEmail();
		tabbedPane.add(panelPreferencesEmail);

		JLabel label = new JLabel(bundleMessages.getString("preferences.tab.email"));
		label.setIconTextGap(5);
		label.setPreferredSize(new Dimension(90, 100));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setVerticalTextPosition(SwingConstants.BOTTOM);
		label.setIcon(IconUtil.getImageIcon(bundleImages.getString("preferences.tab.email.icon")));
		tabbedPane.setTabComponentAt(1, label);
	}

	/**
	 * Inicializar o painel de abas.
	 */
	private void initializeTabPane() {
		tabbedPane = new JTabbedPane();
		add(tabbedPane, BorderLayout.CENTER);
	}

	public int getRefreshRate() {
		return panelPreferencesGeneral.getRefreshRate();
	}

	public String getNotificationSound() {
		return panelPreferencesGeneral.getNotificationSound();
	}

	public void setRefreshRate(int refreshRate) {
		panelPreferencesGeneral.setRefreshRate(refreshRate);
	}

	public void setNotificationSound(String notificationSound) {
		panelPreferencesGeneral.setNotificationSound(notificationSound);
	}

	public String getEmailPassword() {
		return panelPreferencesEmail.getEmailPassword();
	}

	public EmailServerSecurity getEmailSecurity() {
		return panelPreferencesEmail.getEmailSecurity();
	}

	public String getEmailServer() {
		return panelPreferencesEmail.getEmailServer();
	}

	public String getEmailServerPort() {
		return panelPreferencesEmail.getEmailServerPort();
	}

	public String getEmailTemplate() {
		return panelPreferencesEmail.getEmailTemplate();
	}

	public String getEmailUsername() {
		return panelPreferencesEmail.getEmailUsername();
	}

	public void setEmailPassword(String emailPassword) {
		panelPreferencesEmail.setEmailPassword(emailPassword);
	}

	public void setEmailSecurity(EmailServerSecurity emailSecurity) {
		panelPreferencesEmail.setEmailSecurity(emailSecurity);
	}

	public void setEmailServer(String emailServer) {
		panelPreferencesEmail.setEmailServer(emailServer);
	}

	public void setEmailServerPort(int emailServerPort) {
		panelPreferencesEmail.setEmailServerPort(emailServerPort);
	}

	public void setEmailTemplate(String emailTemplate) {
		panelPreferencesEmail.setEmailTemplate(emailTemplate);
	}

	public void setEmailUsername(String emailUsername) {
		panelPreferencesEmail.setEmailUsername(emailUsername);
	}

	public void setEmailSecurityOptions(EmailServerSecurity[] values) {
		panelPreferencesEmail.setEmailSecurityOptions(values);
	}

	public void setEmailOptions(EmailServers[] values) {
		panelPreferencesEmail.setEmailOptions(values);
	}

	public void enableEmailTest() {
		panelPreferencesEmail.enableEmailTest();
	}

	public void disableEmailTest() {
		panelPreferencesEmail.disableEmailTest();
	}

	public void setEmailSender(String emailSender) {
		panelPreferencesEmail.setEmailSender(emailSender);
	}

	public void setEmailSubject(String emailSubject) {
		panelPreferencesEmail.setEmailSubject(emailSubject);
	}

	public String getEmailSubject() {
		return panelPreferencesEmail.getEmailSubject();
	}

	public String getEmailSender() {
		return panelPreferencesEmail.getEmailSender();
	}

	public void setAvailableThemes(Map<String, SkinInfo> allSkins) {
		panelPreferencesVisual.setAvailableThemes(allSkins);
	}

	public void setCurrentTheme(String currentSkin) {
		panelPreferencesVisual.setCurrentTheme(currentSkin);
	}

	public String getTheme() {
		return panelPreferencesVisual.getTheme();
	}

	public String getProxyUsername() {
		return panelPreferencesNetwork.getProxyUsername();
	}

	public void setProxyUsername(String username) {
		panelPreferencesNetwork.setProxyUsername(username);
	}

	public boolean isUseProxy() {
		return panelPreferencesNetwork.isUseProxy();
	}

	public void setUseProxy(boolean use) {
		panelPreferencesNetwork.setUseProxy(use);
	}

	public String getProxyPassword() {
		return panelPreferencesNetwork.getProxyPassword();
	}

	public void setProxyPassword(String pas) {
		panelPreferencesNetwork.setProxyPassword(pas);
	}

	public boolean isSendAutomaticEmails() {
		return panelPreferencesGeneral.isSendAutomaticEmails();
	}

	public void setSendAutomaticEmails(boolean send) {
		panelPreferencesGeneral.setSendAutomaticEmails(send);
	}
}
