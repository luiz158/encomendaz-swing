package com.alienlabz.encomendaz.presenter;

import java.util.List;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

import br.gov.frameworkdemoiselle.stereotype.ViewController;

import com.alienlabz.encomendaz.bus.events.ShowSearchForm;
import com.alienlabz.encomendaz.bus.qualifiers.PostalServiceChanged;
import com.alienlabz.encomendaz.bus.qualifiers.Searched;
import com.alienlabz.encomendaz.business.PersonBC;
import com.alienlabz.encomendaz.business.PostalServiceBC;
import com.alienlabz.encomendaz.business.ServiceBC;
import com.alienlabz.encomendaz.business.TrackingBC;
import com.alienlabz.encomendaz.domain.Service;
import com.alienlabz.encomendaz.domain.Tracking;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.SearchForm;

/**
 * Responsável em tratar os eventos e a inicialização da tela de busca..
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@ViewController
@Singleton
public class SearchPresenter {

	@Inject
	private PostalServiceBC postalServiceBC;

	@Inject
	private ServiceBC serviceBC;

	@Inject
	private PersonBC personBC;

	@Inject
	private TrackingBC trackingBC;

	private SearchForm searchForm;

	/**
	 * Observar o evento que solicita a pesquisa de rastreamentos.
	 * 
	 * @param searchForm Formulário de pesquisa.
	 */
	public void searched(@Observes @Searched SearchForm searchForm) {
		Tracking tracking = new Tracking();
		tracking.setCode(searchForm.getCode());
		tracking.setDescription(searchForm.getDescription());
		tracking.setImportant(searchForm.isImportant());
		tracking.setTaxed(searchForm.isTaxed());
		tracking.setFrom(searchForm.getFrom());
		tracking.setTo(searchForm.getTo());
		tracking.setState(searchForm.getState());
		tracking.setSent(searchForm.getSent());
		tracking.setArchived(searchForm.isArchived());
		Service service = searchForm.getService();
		if (service == null) {
			service = new Service();
		}
		service.setPostalService(searchForm.getPostalService());
		tracking.setService(service);

		searchForm.setResult(trackingBC.search(tracking));
	}

	/**
	 * Observar o evento que solicita a janela de pesquisa de rastreamentos.
	 * 
	 * @param event Evento.
	 */
	public void showSearchForm(@Observes ShowSearchForm event) {
		searchForm = new SearchForm();
		searchForm.pack();
		searchForm.setPostalServicesList(postalServiceBC.findAll());
		searchForm.setFromList(personBC.findAll());
		searchForm.setToList(personBC.findAll());
		searchForm.setStatesList(trackingBC.getAllStates());
		SwingUtil.center(searchForm);
		searchForm.setVisible(true);
	}

	/**
	 * Capturar o evento lançado quando o usuário muda o serviço postal.
	 * 
	 * @param event Evento.
	 */
	public void postalServiceChangedFromSearchWindow(@Observes @PostalServiceChanged final SearchForm seachForm) {
		if (seachForm.getPostalService() != null) {
			new AsyncTask<List<Service>, Void>() {

				@Override
				protected List<Service> doTask() {
					return serviceBC.findByPostalService(seachForm.getPostalService().getId());
				}

				protected void onPostExecute(java.util.List<Service> result) {
					searchForm.setServicesList(result);
				}

			}.execute();
		}
	}

}
