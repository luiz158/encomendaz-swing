package com.alienlabz.encomendaz.util;

import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.Stopped;
import com.alienlabz.encomendaz.bus.events.Working;
import com.alienlabz.encomendaz.exceptions.ExceptionHandler;


abstract public class AsyncTask<T, V> extends SwingWorker<T, V> {

	@Override
	final protected T doInBackground() throws Exception {
		EventManager.fire(new Working());
		onPreExecute();
		return doTask();
	}

	@Override
	protected void done() {
		try {
			onPostExecute(get());
		} catch (final InterruptedException e) {
			onException(e);
		} catch (final ExecutionException e) {
			onException(e.getCause());
		}
		EventManager.fire(new Stopped());
	}

	protected abstract T doTask();

	protected void onException(final Throwable throwable) {
		ExceptionHandler.handle(throwable);
	}

	protected void onPostExecute(final T result) {
	}

	protected void onPreExecute() {
	}

}
