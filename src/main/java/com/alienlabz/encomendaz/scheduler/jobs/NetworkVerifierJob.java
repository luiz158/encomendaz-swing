package com.alienlabz.encomendaz.scheduler.jobs;

import java.io.IOException;


import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.alienlabz.encomendaz.bus.EventManager;
import com.alienlabz.encomendaz.bus.events.NetworkOff;
import com.alienlabz.encomendaz.bus.events.NetworkOn;

/**
 * Tarefa de agendamento que verifica se há conexão com os serviços postais.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class NetworkVerifierJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		HttpGet httpget = new HttpGet("http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=");
		DefaultHttpClient client = new DefaultHttpClient();
		try {
			HttpResponse response = client.execute(httpget);
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				EventManager.fire(new NetworkOff());
			} else {
				EventManager.fire(new NetworkOn());
			}
		} catch (ClientProtocolException e) {
			EventManager.fire(new NetworkOff());
		} catch (IOException e) {
			EventManager.fire(new NetworkOff());
		}
	}
}
