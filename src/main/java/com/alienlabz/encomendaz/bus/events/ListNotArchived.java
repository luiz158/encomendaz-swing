package com.alienlabz.encomendaz.bus.events;

/**
 * Evento lançado quando há a necessidade de exibir os rastreamentos não arquivados.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class ListNotArchived {

}
