package com.alienlabz.encomendaz.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.alfredlibrary.utilitarios.midia.Audio;

/**
 * Utilitário para tocar sons.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
public class AudioPlayer {

	synchronized public static void play(final String file) {
		AudioInputStream audio;
		try {
			FileInputStream is = new FileInputStream(file);
			audio = AudioSystem.getAudioInputStream(is);
			Audio.executar(audio);
		} catch (UnsupportedAudioFileException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	synchronized public static void play(final InputStream is) {
		AudioInputStream audio;
		try {
			audio = AudioSystem.getAudioInputStream(is);
			Audio.executar(audio);
		} catch (UnsupportedAudioFileException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}