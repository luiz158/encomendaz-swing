package com.alienlabz.encomendaz.presenter;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

import com.alienlabz.encomendaz.bus.events.CreateBackup;
import com.alienlabz.encomendaz.bus.events.ShowBackupWindow;
import com.alienlabz.encomendaz.bus.qualifiers.Deleted;
import com.alienlabz.encomendaz.bus.qualifiers.Recovery;
import com.alienlabz.encomendaz.business.BackupBC;
import com.alienlabz.encomendaz.domain.Backup;
import com.alienlabz.encomendaz.message.Messages;
import com.alienlabz.encomendaz.util.AsyncTask;
import com.alienlabz.encomendaz.util.SwingUtil;
import com.alienlabz.encomendaz.view.window.BackupForm;

import br.gov.frameworkdemoiselle.annotation.Name;
import br.gov.frameworkdemoiselle.stereotype.ViewController;
import br.gov.frameworkdemoiselle.util.ResourceBundle;

/**
 * Responsável em tratar os eventos oriundos do formulário de backup.
 * 
 * @author Marlon Silva Carvalho
 * @since 3.0.0
 */
@ViewController
@Singleton
public class BackupPresenter {

	private BackupForm backupForm;

	@Inject
	private BackupBC backupBC;
	
	@Inject
	@Name("messages")
	private ResourceBundle bundleMessages;

	/**
	 * Exibir o formulário para realizar o backup.
	 * 
	 * @param event Evento.
	 */
	public void showBackupWindow(@Observes ShowBackupWindow event) {
		backupForm = new BackupForm();
		backupForm.pack();
		SwingUtil.center(backupForm);
		backupForm.setBackupList(backupBC.findAvailableBackups());
		backupForm.setVisible(true);
	}

	/**
	 * Capturar o evento que solicita para remover um backup.
	 * 
	 * @param backup Backup a ser removido.
	 */
	public void recovery(@Observes @Recovery final Backup backup) {
		new AsyncTask<Void, Void>() {

			protected void onPreExecute() {
				backupForm.setBusy(true);
			}

			@Override
			protected Void doTask() {
				backupBC.recovery(backup);
				return null;
			}

			protected void onPostExecute(Void result) {
				backupForm.setBusy(false);
				Messages.info(bundleMessages.getString("form.backup.success.load"));
			}

			protected void onException(Throwable throwable) {
				backupForm.setBusy(false);
				Messages.error(bundleMessages.getString("form.backup.fail.load"));
			}
		}.execute();
	}

	/**
	 * Capturar o evento que solicita para remover um backup.
	 * 
	 * @param backup Backup a ser removido.
	 */
	public void deleteBackup(@Observes @Deleted Backup backup) {
		backupBC.deleteBackup(backup);
		backupForm.setBackupList(backupBC.findAvailableBackups());
	}

	/**
	 * Realizar o backup do sistema.
	 * 
	 * @param event Evento.
	 */
	public void createBackup(@Observes CreateBackup event) {

		new AsyncTask<Void, Void>() {

			protected void onPreExecute() {
				backupForm.setBusy(true);
			}

			@Override
			protected Void doTask() {
				backupBC.createBackup();
				return null;
			}

			protected void onPostExecute(Void result) {
				backupForm.setBackupList(backupBC.findAvailableBackups());
				backupForm.setBusy(false);
			}

			protected void onException(Throwable throwable) {
				backupForm.setBusy(false);
			}
		}.execute();
	}

}
